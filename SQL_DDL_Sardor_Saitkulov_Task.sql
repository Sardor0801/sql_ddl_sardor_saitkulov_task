CREATE DATABASE auction_db;
-----------------------------------------------------------------------------------------------
CREATE SCHEMA auction;

CREATE TABLE auction.address (
	address_id SERIAL PRIMARY KEY,
	address varchar(250) NOT NULL,
	postal_code varchar(20)
);

CREATE TABLE auction.auction (
	auction_id SERIAL PRIMARY KEY,
	auction_date date NOT NULL CHECK (auction_date > '2000-01-01'),
	auction_time time NOT NULL,
	address_id BIGINT NOT NULL REFERENCES auction.address
);

CREATE TABLE auction.buyer (
	buyer_id SERIAL PRIMARY KEY,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	full_name varchar(200) GENERATED ALWAYS AS (first_name || ' ' || last_name) STORED NOT NULL,
	email varchar(150) NOT NULL,
	phone varchar(20),
	gender char NOT NULL CHECK (gender IN ('M', 'F')),
	address_id BIGINT NOT NULL REFERENCES auction.address,
	CONSTRAINT "UNQ_buyer_email" UNIQUE (email),
	CONSTRAINT "UNQ_buyer_phone" UNIQUE (phone)
);

CREATE TABLE auction.seller (
	seller_id SERIAL PRIMARY KEY,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	full_name varchar(200) GENERATED ALWAYS AS (first_name || ' ' || last_name) STORED NOT NULL,
	email varchar(150) NOT NULL,
	phone varchar(20),
	gender char NOT NULL CHECK (gender IN ('M', 'F')),
	address_id BIGINT NOT NULL REFERENCES auction.address,
	CONSTRAINT "UNQ_seller_email" UNIQUE (email),
	CONSTRAINT "UNQ_seller_phone" UNIQUE (phone)
);

CREATE TABLE auction.item (
	item_id SERIAL PRIMARY KEY,
	item_name varchar(250) NOT NULL,
	item_description text DEFAULT 'no description',
	item_price decimal(10,2) NOT NULL CHECK (item_price >= 0),
	seller_id BIGINT NOT NULL REFERENCES auction.seller
);

CREATE TABLE auction.category (
	category_id SERIAL PRIMARY KEY,
	description varchar(250) DEFAULT 'no category'
);

CREATE TABLE auction.item_category (
	item_id BIGINT NOT NULL REFERENCES auction.item,
	category_id BIGINT NOT NULL REFERENCES auction.category
);

CREATE TABLE auction.lot (
	lot_id SERIAL PRIMARY KEY,
	lot_number integer NOT NULL,
	item_id BIGINT NOT NULL REFERENCES auction.item
);

CREATE TABLE auction.auction_item (
	auction_item_id SERIAL PRIMARY KEY,
	auction_id BIGINT NOT NULL REFERENCES auction.auction,
	lot_id BIGINT NOT NULL REFERENCES auction.lot
);

CREATE TABLE auction.purchase (
	purchase_id SERIAL PRIMARY KEY,
	purchase_actual_price decimal(10,2) NOT NULL CHECK (purchase_actual_price >= 0),
	buyer_id BIGINT NOT NULL REFERENCES auction.buyer,
	auction_item_id BIGINT NOT NULL REFERENCES auction.auction_item
);

CREATE TABLE auction.purchase_method (
	purchase_method_id SERIAL PRIMARY KEY,
	description varchar(250) NOT NULL,
	purchase_id BIGINT NOT NULL REFERENCES auction.purchase
);





INSERT INTO auction.address (address, postal_code)
VALUES ('123 Main Street', '12345'),
       ('456 Elm Avenue', '67890'),
	   ('789 Oak Street', '54321'),
       ('321 Pine Avenue', '98765');

INSERT INTO auction.auction (auction_date, auction_time, address_id)
VALUES ('2023-10-29', '10:00:00', 1),
       ('2023-11-05', '14:30:00', 2);
	   
INSERT INTO auction.buyer (first_name, last_name, email, phone, gender, address_id)
VALUES ('John', 'Doe', 'johndoe@example.com', '1234567890', 'M', 3),
       ('Jane', 'Smith', 'janesmith@example.com', '9876543210', 'F', 4);
	   
INSERT INTO auction.seller (first_name, last_name, email, phone, gender, address_id)
VALUES ('Michael', 'Johnson', 'michaeljohnson@example.com', '5551234567', 'M', 1),
       ('Emily', 'Davis', 'emilydavis@example.com', '5559876543', 'F', 4);
	 
INSERT INTO auction.item (item_name, item_description, item_price, seller_id)
VALUES ('Painting', 'Beautiful landscape painting', 100.00, 1),
       ('Watch', 'Stainless steel wristwatch', 250.00, 2);
	   
INSERT INTO auction.lot (lot_number, item_id)
VALUES (1, 2),
       (2, 1);
	   
INSERT INTO auction.category (description)
VALUES ('Art'),
       ('Jewelry');

INSERT INTO auction.item_category (item_id, category_id)
VALUES (1, 1),
       (2, 2);

INSERT INTO auction.auction_item (auction_id, lot_id)
VALUES (1, 1),
       (2, 2);	
	   
INSERT INTO auction.purchase (purchase_actual_price, buyer_id,auction_item_id)
VALUES (80.00, 1, 2),
       (200.00, 2, 1);


INSERT INTO auction.purchase_method (description, purchase_id)
VALUES ('Credit Card', 1),
       ('Cash', 2);


ALTER TABLE auction.address 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.auction 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.buyer 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.seller
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.lot 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.item 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.category 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.item_category 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.purchase 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.purchase_method 
ADD COLUMN record_ts DATE DEFAULT current_date;

ALTER TABLE auction.auction_item 
ADD COLUMN record_ts DATE DEFAULT current_date;